package main

import (
	"net/http"
	"os"

	"log"

	"./handlers"
	"github.com/gorilla/pat"
	"github.com/gorilla/sessions"
	"github.com/markbates/goth"
	"github.com/markbates/goth/gothic"
	"github.com/markbates/goth/providers/google"
)

func main() {

	l := log.New(os.Stdout, "users-frontend ", log.LstdFlags)

	uh := handlers.NewUsers(l)

	key := "Secret-session-key" // Replace with your SESSION_SECRET or similar
	maxAge := 86400 * 30        // 30 days
	isProd := false             // Set to true when serving over https

	store := sessions.NewCookieStore([]byte(key))
	store.MaxAge(maxAge)
	store.Options.Path = "/"
	store.Options.HttpOnly = true // HttpOnly should always be enabled
	store.Options.Secure = isProd

	gothic.Store = store

	loginProvider := google.New("175664330757-emo4ns1qp5dvo677s8p6tn4hkiobm32b.apps.googleusercontent.com", "QIqymwL49algCqNXFbPWihrj", "http://185.106.209.60.xip.io:3000/auth/loginProvider/callback/login", "email", "profile")
	loginProvider.SetName("loginProvider")
	registerProvider := google.New("175664330757-kv3ijlkb5qj799mj3bs0ei2rsno9tnp9.apps.googleusercontent.com", "XFvrYbtYhEpD940hCVpbKFFp", "http://185.106.209.60.xip.io:3000/auth/registerProvider/callback/register", "email", "profile")
	registerProvider.SetName("registerProvider")
	goth.UseProviders(loginProvider, registerProvider)

	p := pat.New()

	// Google API Handlers
	p.Get("/auth/{provider}/callback/login", uh.GoogleCompleteAuthSignIn)
	p.Get("/logout/{provider}", uh.GoogleLogout)
	p.Get("/auth/{provider}/login", uh.GoogleAuthSignIn)
	p.Get("/auth/{provider}/callback/register", uh.GoogleCompleteAuthSignUp)
	p.Get("/auth/{provider}/register", uh.GoogleAuthSignUp)

	// Local Handlers

	p.Post("/login", uh.Login)
	p.Post("/register", uh.Register)
	p.Get("/profile/edit", uh.EditProfile)
	p.Post("/profile/update", uh.UpdateProfile)
	p.Get("/profile", uh.Profile)

	p.Post("/logout", uh.Logout)

	p.Get("/signup", uh.SignUp)
	p.Post("/signup", uh.SignUp)
	p.Get("/forgotpassword", uh.ForgotPassword)
	p.Post("/forgotpassword", uh.ForgotPasswordPost)
	p.Get("/resetpassword", uh.ResetPassword)
	p.Post("/resetpassword", uh.ResetPasswordPost)
	p.Get("/", uh.SignIn)
	p.Post("/", uh.SignIn)

	http.Handle("/", p)

	log.Println("listening on localhost:3000")
	log.Fatal(http.ListenAndServe(handlers.BindAddr, p))
}
