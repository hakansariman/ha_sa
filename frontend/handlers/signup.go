package handlers

import (
	"bytes"
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
)

func (u *Users) SignUp(res http.ResponseWriter, req *http.Request) {
	res.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate") // HTTP 1.1.
	res.Header().Set("Pragma", "no-cache")                                   // HTTP 1.0.
	res.Header().Set("Expires", "0")

	t, err := template.ParseFiles("templates/signup.html")
	if err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	t.Execute(res, AuthError{false, ""})
	// Proxies.
}

func (u *Users) Register(res http.ResponseWriter, req *http.Request) {
	res.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate") // HTTP 1.1.
	res.Header().Set("Pragma", "no-cache")                                   // HTTP 1.0.
	res.Header().Set("Expires", "0")

	username := req.FormValue("username")
	password := req.FormValue("password")
	if username == "" || password == "" {
		u.redirectToSignUpPage(res, AuthError{true, "Username and password should be entered."})
		return
	}
	user := User{
		Username:     username,
		Password:     password,
		IsGoogleUser: false,
	}

	var buf bytes.Buffer
	err := ToJSON(user, &buf)
	if err != nil {
		u.l.Println("[DEBUG] Unable to encoding json")
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	resp, err := http.Post(fmt.Sprintf("%v/users", backendService), "application/json", &buf)
	if err != nil {
		u.l.Printf("[DEBUG] http post error %v", err.Error())
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	if resp.StatusCode != http.StatusOK {
		var errorMsg GenericError
		err := FromJSON(&errorMsg, resp.Body)
		if err != nil {
			body, _ := ioutil.ReadAll(resp.Body)
			u.l.Printf("[DEBUG] Unable to decode json. Body: %v STATUS: %v", string(body), resp.StatusCode)
			res.WriteHeader(http.StatusInternalServerError)
			return
		}
		// return to signin page
		u.redirectToSignUpPage(res, AuthError{true, errorMsg.Message})
		return
	}
	// redirect to profile
	http.Redirect(res, req, "/", 302)
	res.Header().Set("Location", "/")
}
