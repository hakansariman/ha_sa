package handlers

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"net/http/cookiejar"

	"github.com/dgrijalva/jwt-go"
)

// User defines the structure for an API user
type User struct {
	// the id for the user
	//
	// required: false
	// min: 1
	ID int `json:"id"` // Unique identifier for the user

	// the username for this user
	//
	// required: true
	// max length: 255
	Username string `json:"username" validate:"required"`

	// the password for this user
	//
	// required: true
	// max length: 255
	Password string `json:"password" validate:"required"`

	// the fullname for this user
	//
	// required: false
	// max length: 255
	FullName string `json:"fullname"`

	// the address for this user
	//
	// required: false
	// max length: 255
	Address string `json:"address"`

	// the fullname for this user
	//
	// required: false
	// max length: 255
	Phone string `json:"phone"`

	// is user authenticated by Google API
	//
	// required: false
	IsGoogleUser bool `json:"isGoogleUser"`
}

// PasswordReset defines the structure for an reset password request
type PasswordReset struct {
	// the username for this user
	//
	// required: false
	// max length: 255
	Username string `json:"username" `

	Password string `json:"password"`

	// CallbackAddr is the IP:Port data which mail be created
	CallbackAddr string `json:"callbackaddr"`
}

// PasswordResetTemplate defines the structure for an reset password request for frontend template
type PasswordResetTemplate struct {
	Token string `json:"token" validate:"required"`
}

// GenericError is a generic error message returned by a server
type GenericError struct {
	Message string `json:"message"`
}

// Users handler for getting and updating users
type Users struct {
	l  *log.Logger
	cs *CookieStore
}

// NewUsers returns a new users handler with the given logger
func NewUsers(l *log.Logger) *Users {
	cs := NewCookieStore()
	return &Users{l, cs}
}

func getUserIDFromToken(tokenStr string) (int, error) {
	token, _, err := new(jwt.Parser).ParseUnverified(tokenStr, jwt.MapClaims{})
	if err != nil {
		return -1, err
	}

	if _, ok := token.Claims.(jwt.MapClaims); !ok {
		return -1, fmt.Errorf("invalid token claims")
	}
	claims := token.Claims.(jwt.MapClaims)
	if _, ok := claims["id"]; !ok {
		return -1, fmt.Errorf("invalid token claims")
	}
	return int(claims["id"].(float64)), nil
}

func fetchTokenFromCookies(cookies []*http.Cookie) (string, error) {
	var tokenStr string
	for _, cookie := range cookies {
		if cookie.Name == "token" {
			tokenStr = cookie.Value
		}
	}
	if tokenStr == "" {
		return "", fmt.Errorf("token not found")
	}
	return tokenStr, nil
}

func fetchUserData(userID int, token string) (User, error) {
	cookieJar, _ := cookiejar.New(nil)

	client := &http.Client{
		Jar: cookieJar,
	}
	var user User
	req, err := http.NewRequest("GET", fmt.Sprintf("%v/users/%v", backendService, userID), nil)
	if err != nil {
		return user, err
	}
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
	resp, err := client.Do(req)
	if err != nil {
		return user, err
	}
	if resp.StatusCode != http.StatusOK {
		var errorMsg GenericError
		err := FromJSON(&errorMsg, resp.Body)
		if err != nil {
			return user, err
		}
		return user, fmt.Errorf(errorMsg.Message)
	}
	err = FromJSON(&user, resp.Body)
	if err != nil {
		return user, err
	}
	return user, nil
}

func (u *Users) redirectToSignInPage(rw http.ResponseWriter, authErr AuthError) {
	t, err := template.ParseFiles("templates/signin.html")
	if err != nil {
		u.l.Println("[DEBUG] Unable to parse signin.html")
		rw.WriteHeader(http.StatusInternalServerError)
		return
	}
	t.Execute(rw, authErr)
}

func (u *Users) redirectToSignUpPage(rw http.ResponseWriter, authErr AuthError) {
	t, err := template.ParseFiles("templates/signup.html")
	if err != nil {
		u.l.Println("[DEBUG] Unable to parse signup.html")
		rw.WriteHeader(http.StatusInternalServerError)
		return
	}
	t.Execute(rw, authErr)
}
