package handlers

import (
	"net/http"
	"time"
)

// Logout logouts user and reset users cookie
func (u *Users) Logout(res http.ResponseWriter, req *http.Request) {
	// set token to cookie
	cookie := http.Cookie{Name: "token", Value: "", Expires: time.Now()}
	http.SetCookie(res, &cookie)
	// delete cookie if it exists in cookie store
	u.cs.Delete(req.RemoteAddr)
	// redirect main page
	res.Header().Set("Location", "/")
	res.WriteHeader(http.StatusTemporaryRedirect)
}
