package handlers

import (
	"bytes"
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
	"time"
)

func (u *Users) SignIn(res http.ResponseWriter, req *http.Request) {
	res.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate") // HTTP 1.1.
	res.Header().Set("Pragma", "no-cache")                                   // HTTP 1.0.
	res.Header().Set("Expires", "0")
	t, err := template.ParseFiles("templates/signin.html")
	if err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	t.Execute(res, AuthError{false, ""})
}

func (u *Users) Login(res http.ResponseWriter, req *http.Request) {
	res.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate") // HTTP 1.1.
	res.Header().Set("Pragma", "no-cache")                                   // HTTP 1.0.
	res.Header().Set("Expires", "0")
	username := req.FormValue("username")
	password := req.FormValue("password")
	if username == "" || password == "" {
		u.redirectToSignInPage(res, AuthError{true, "Username and password should be entered."})
		return
	}
	user := User{
		Username:     username,
		Password:     password,
		IsGoogleUser: false,
	}

	var buf bytes.Buffer
	err := ToJSON(user, &buf)
	if err != nil {
		u.l.Println("[DEBUG] Unable to encoding json")
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	resp, err := http.Post(fmt.Sprintf("%v/signin", backendService), "application/json", &buf)
	if err != nil {
		u.l.Printf("[DEBUG] http post error %v", err.Error())
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	if resp.StatusCode != http.StatusOK {
		var errorMsg GenericError
		err := FromJSON(&errorMsg, resp.Body)
		if err != nil {
			body, _ := ioutil.ReadAll(resp.Body)
			u.l.Printf("[DEBUG] Unable to decode json. Body: %v", string(body))
			res.WriteHeader(http.StatusInternalServerError)
			return
		}
		// return to signin page
		u.redirectToSignInPage(res, AuthError{true, errorMsg.Message})
		return
	}
	// fetch token from cookies
	cookies := resp.Cookies()
	expiration := time.Now().Add(time.Minute * 45)
	tokenStr, err := fetchTokenFromCookies(cookies)
	if err != nil {
		u.redirectToSignInPage(res, AuthError{true, err.Error()})
		return
	}
	// set token to cookie
	cookie := http.Cookie{Name: "token", Value: tokenStr, Expires: expiration}
	http.SetCookie(res, &cookie)

	// redirect to profile
	http.Redirect(res, req, "/profile", 302)
	res.Header().Set("Location", "/profile")
}
