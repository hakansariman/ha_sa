package handlers

import (
	"net/http"
	"sync"
)

// CookieStore stores cookies for signed users via Google API
type CookieStore struct {
	cookies map[string]http.Cookie
	mtx     sync.RWMutex
}

// NewCookieStore creates a NewCookieStore
func NewCookieStore() *CookieStore {
	cs := new(CookieStore)
	cs.cookies = make(map[string]http.Cookie)
	return cs
}

// Get gets the cookie for specified remote address
func (cs *CookieStore) Get(remoteAddr string) (http.Cookie, bool) {
	cs.mtx.RLock()
	c, ok := cs.cookies[remoteAddr]
	cs.mtx.RUnlock()
	return c, ok
}

// SetCookie sets the given cookie for specified remote address
func (cs *CookieStore) SetCookie(remoteAddr string, cookie http.Cookie) {
	cs.mtx.Lock()
	cs.cookies[remoteAddr] = cookie
	cs.mtx.Unlock()
}

// Delete deletes the cookie for specified remote address
func (cs *CookieStore) Delete(remoteAddr string) {
	cs.mtx.Lock()
	delete(cs.cookies, remoteAddr)
	cs.mtx.Unlock()
}
