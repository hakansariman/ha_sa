package handlers

import (
	"bytes"
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
)

func (u *Users) Profile(res http.ResponseWriter, req *http.Request) {
	res.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate") // HTTP 1.1.
	res.Header().Set("Pragma", "no-cache")                                   // HTTP 1.0.
	res.Header().Set("Expires", "0")
	cookies := req.Cookies()
	var tokenStr string
	tokenStr, err := fetchTokenFromCookies(cookies)
	if err != nil {
		cookie, ok := u.cs.Get(req.RemoteAddr)
		if !ok {
			u.redirectToSignInPage(res, AuthError{true, err.Error()})
			return
		}
		tokenStr = cookie.Value
	}
	userID, err := getUserIDFromToken(tokenStr)
	if err != nil {
		u.redirectToSignInPage(res, AuthError{true, err.Error()})
		return
	}
	userData, err := fetchUserData(userID, tokenStr)
	if err != nil {
		u.redirectToSignInPage(res, AuthError{true, err.Error()})
		return
	}
	t, err := template.ParseFiles("templates/profile.html")
	if err != nil {
		u.l.Println("[DEBUG] Unable to parse signin.html")
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	t.Execute(res, userData)

}

func (u *Users) EditProfile(res http.ResponseWriter, req *http.Request) {
	res.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate") // HTTP 1.1.
	res.Header().Set("Pragma", "no-cache")                                   // HTTP 1.0.
	res.Header().Set("Expires", "0")
	cookies := req.Cookies()
	var tokenStr string
	tokenStr, err := fetchTokenFromCookies(cookies)
	if err != nil {
		cookie, ok := u.cs.Get(req.RemoteAddr)
		if !ok {
			u.redirectToSignInPage(res, AuthError{true, err.Error()})
			return
		}
		tokenStr = cookie.Value
	}
	userID, err := getUserIDFromToken(tokenStr)
	if err != nil {
		u.redirectToSignInPage(res, AuthError{true, err.Error()})
		return
	}
	userData, err := fetchUserData(userID, tokenStr)
	if err != nil {
		u.redirectToSignInPage(res, AuthError{true, err.Error()})
		return
	}
	t, err := template.ParseFiles("templates/editProfile.html")
	if err != nil {
		u.l.Println("[DEBUG] Unable to parse editProfile.html")
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	t.Execute(res, userData)

}

func (u *Users) UpdateProfile(res http.ResponseWriter, req *http.Request) {
	res.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate") // HTTP 1.1.
	res.Header().Set("Pragma", "no-cache")                                   // HTTP 1.0.
	res.Header().Set("Expires", "0")
	username := req.FormValue("username")
	fullName := req.FormValue("name")
	address := req.FormValue("address")
	phone := req.FormValue("phone")
	isGoogleUser := req.FormValue("isGoogleUser")
	if username == "" {
		u.EditProfile(res, req)
		return
	}

	cookies := req.Cookies()
	var tokenStr string
	tokenStr, err := fetchTokenFromCookies(cookies)
	if err != nil {
		cookie, ok := u.cs.Get(req.RemoteAddr)
		if !ok {
			u.EditProfile(res, req)
			return
		}
		tokenStr = cookie.Value
	}
	userID, err := getUserIDFromToken(tokenStr)
	if err != nil {
		u.EditProfile(res, req)
		return
	}

	user := User{
		ID:           userID,
		Username:     username,
		FullName:     fullName,
		Address:      address,
		Phone:        phone,
		IsGoogleUser: isGoogleUser == "true",
	}

	var buf bytes.Buffer
	err = ToJSON(user, &buf)
	if err != nil {
		u.l.Println("[DEBUG] Unable to encoding json")
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	reqAPI, err := http.NewRequest("PUT", fmt.Sprintf("%v/users", backendService), &buf)
	if err != nil {
		u.l.Printf("[DEBUG] http post error %v", err.Error())
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	reqAPI.Header.Set("Authorization", fmt.Sprintf("Bearer %s", tokenStr))

	resp, err := http.DefaultClient.Do(reqAPI)
	if err != nil {
		u.l.Printf("[DEBUG] http post error to update profile %v", err.Error())
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	if resp.StatusCode != http.StatusOK && resp.StatusCode != http.StatusNoContent {
		var errorMsg GenericError
		err := FromJSON(&errorMsg, resp.Body)
		if err != nil {
			body, _ := ioutil.ReadAll(resp.Body)
			u.l.Printf("[DEBUG] Unable to decode json. Body: %v", string(body))
			res.WriteHeader(http.StatusInternalServerError)
			return
		}
		// return to signin page
		u.redirectToSignInPage(res, AuthError{true, errorMsg.Message})
		return
	}
	// redirect to profile
	http.Redirect(res, req, "/profile", 302)
	res.Header().Set("Location", "/profile")
}
