package handlers

// AuthError is used when showing error messages on templates
type AuthError struct {
	ThereIsError bool
	Error        string
}

// backendService is the address of backend
var backendService = "http://127.0.0.1:9090"

// BindAddr is the address of working HTTP server
var BindAddr = "185.106.209.60:3000"
