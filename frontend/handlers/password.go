package handlers

import (
	"bytes"
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
)

// ForgotPassword parses forgotPassword.html
func (u *Users) ForgotPassword(res http.ResponseWriter, req *http.Request) {
	res.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate") // HTTP 1.1.
	res.Header().Set("Pragma", "no-cache")                                   // HTTP 1.0.
	res.Header().Set("Expires", "0")
	t, err := template.ParseFiles("templates/forgotPassword.html")
	if err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	t.Execute(res, AuthError{false, ""})
}

// ForgotPassword handles post operation for forgot password
// it fetchs the mail address from user and send to backend service
func (u *Users) ForgotPasswordPost(res http.ResponseWriter, req *http.Request) {
	// disable browser caching
	res.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate") // HTTP 1.1.
	res.Header().Set("Pragma", "no-cache")                                   // HTTP 1.0.
	res.Header().Set("Expires", "0")

	// fetch mail address
	username := req.FormValue("username")
	if username == "" {
		u.redirectToForgotPassword(res, AuthError{true, "Email should be entered."})
		return
	}
	// prepare request body
	var pr PasswordReset
	pr.Username = username
	pr.CallbackAddr = BindAddr

	var buf bytes.Buffer
	err := ToJSON(pr, &buf)
	if err != nil {
		u.l.Println("[DEBUG] Unable to encoding json")
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	// prepare request
	reqAPI, err := http.NewRequest("POST", fmt.Sprintf("%v/forgotpassword", backendService), &buf)
	if err != nil {
		u.l.Printf("[DEBUG] http post error %v", err.Error())
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	resp, err := http.DefaultClient.Do(reqAPI)
	if err != nil {
		u.l.Printf("[DEBUG] http post error to forgot password %v", err.Error())
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	if resp.StatusCode != http.StatusOK {
		var errorMsg GenericError
		err := FromJSON(&errorMsg, resp.Body)
		if err != nil {
			body, _ := ioutil.ReadAll(resp.Body)
			u.l.Printf("[DEBUG] Unable to decode json. Body: %v", string(body))
			res.WriteHeader(http.StatusInternalServerError)
			return
		}
		// return to signin page
		u.redirectToSignInPage(res, AuthError{true, errorMsg.Message})
		return
	}
	// redirect to forgotpassword page
	u.redirectToForgotPassword(res, AuthError{true, "Verification link has been sent to specified email."})
}

// ResetPassword parses resetPassword.html
func (u *Users) ResetPassword(res http.ResponseWriter, req *http.Request) {
	res.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate") // HTTP 1.1.
	res.Header().Set("Pragma", "no-cache")                                   // HTTP 1.0.
	res.Header().Set("Expires", "0")
	token := req.FormValue("token")
	t, err := template.ParseFiles("templates/resetPassword.html")
	if err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	t.Execute(res, PasswordResetTemplate{token})
}

// ResetPasswordPost handles post operation for reset password
// it fetchs the new password from user and send to backend service
func (u *Users) ResetPasswordPost(res http.ResponseWriter, req *http.Request) {
	res.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate") // HTTP 1.1.
	res.Header().Set("Pragma", "no-cache")                                   // HTTP 1.0.
	res.Header().Set("Expires", "0")

	token := req.FormValue("token")
	password := req.FormValue("password")
	if token == "" || password == "" {
		u.redirectToResetPassword(res, PasswordResetTemplate{token})
		return
	}
	// prepare request body
	var pr PasswordReset
	pr.Password = password
	var buf bytes.Buffer

	err := ToJSON(pr, &buf)
	if err != nil {
		u.l.Println("[DEBUG] Unable to encoding json")
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	reqAPI, err := http.NewRequest("POST", fmt.Sprintf("%v/resetpassword", backendService), &buf)
	if err != nil {
		u.l.Printf("[DEBUG] http post error %v", err.Error())
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	reqAPI.Header.Set("Authorization", fmt.Sprintf("Bearer %v", token))

	resp, err := http.DefaultClient.Do(reqAPI)
	if err != nil {
		u.l.Printf("[DEBUG] http post error to forgot password %v", err.Error())
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	if resp.StatusCode != http.StatusOK {
		var errorMsg GenericError
		err := FromJSON(&errorMsg, resp.Body)
		if err != nil {
			body, _ := ioutil.ReadAll(resp.Body)
			u.l.Printf("[DEBUG] Unable to decode json. Body: %v", string(body))
			res.WriteHeader(http.StatusInternalServerError)
			return
		}
		// return to signin page
		u.redirectToSignInPage(res, AuthError{true, errorMsg.Message})
		return
	}
	// redirect to sign in page
	u.redirectToSignInPage(res, AuthError{true, "Password updated successfully."})
}

func (u *Users) redirectToForgotPassword(rw http.ResponseWriter, authErr AuthError) {
	t, err := template.ParseFiles("templates/forgotPassword.html")
	if err != nil {
		u.l.Println("[DEBUG] Unable to parse forgotPassword.html")
		rw.WriteHeader(http.StatusInternalServerError)
		return
	}
	t.Execute(rw, authErr)
}

func (u *Users) redirectToResetPassword(rw http.ResponseWriter, prt PasswordResetTemplate) {
	t, err := template.ParseFiles("templates/resetPassword.html")
	if err != nil {
		u.l.Println("[DEBUG] Unable to parse resetPassword.html")
		rw.WriteHeader(http.StatusInternalServerError)
		return
	}
	t.Execute(rw, prt)
}
