package handlers

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/markbates/goth/gothic"
)

func (u *Users) GoogleCompleteAuthSignIn(res http.ResponseWriter, req *http.Request) {

	user, err := gothic.CompleteUserAuth(res, req)
	if err != nil {
		u.redirectToSignInPage(res, AuthError{true, err.Error()})
		return
	}
	userData := User{
		Username:     user.Email,
		Password:     user.UserID,
		IsGoogleUser: true,
	}
	var buf bytes.Buffer
	err = ToJSON(userData, &buf)
	if err != nil {
		u.l.Println("[DEBUG] Unable to encoding json")
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	resp, err := http.Post(fmt.Sprintf("%v/signin", backendService), "application/json", &buf)
	if err != nil {
		u.l.Printf("[DEBUG] http post error %v", err.Error())
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	if resp.StatusCode != http.StatusOK {
		var errorMsg GenericError
		err := FromJSON(&errorMsg, resp.Body)
		if err != nil {
			body, _ := ioutil.ReadAll(resp.Body)
			u.l.Printf("[DEBUG] Unable to decode json. Body: %v", string(body))
			res.WriteHeader(http.StatusInternalServerError)
			return
		}
		// return to signin page
		u.redirectToSignInPage(res, AuthError{true, errorMsg.Message})
		return
	}
	// fetch token from cookies
	cookies := resp.Cookies()
	expiration := time.Now().Add(time.Minute * 45)
	tokenStr, err := fetchTokenFromCookies(cookies)
	if err != nil {
		u.redirectToSignInPage(res, AuthError{true, err.Error()})
		return
	}
	// set token to cookie
	cookie := http.Cookie{Name: "token", Value: tokenStr, Expires: expiration}
	http.SetCookie(res, &cookie)
	u.cs.SetCookie(req.RemoteAddr, cookie)

	// redirect to profile
	http.Redirect(res, req, "/profile", 302)
	res.Header().Set("Location", "/profile")
}

func (u *Users) GoogleCompleteAuthSignUp(res http.ResponseWriter, req *http.Request) {

	user, err := gothic.CompleteUserAuth(res, req)
	if err != nil {
		u.redirectToSignInPage(res, AuthError{true, err.Error()})
		return
	}
	userData := User{
		Username:     user.Email,
		Password:     user.UserID,
		IsGoogleUser: true,
	}
	var buf bytes.Buffer
	err = ToJSON(userData, &buf)
	if err != nil {
		u.l.Println("[DEBUG] Unable to encoding json")
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	resp, err := http.Post(fmt.Sprintf("%v/users", backendService), "application/json", &buf)
	if err != nil {
		u.l.Printf("[DEBUG] http post error %v", err.Error())
		res.WriteHeader(http.StatusInternalServerError)
		return
	}
	if resp.StatusCode != http.StatusOK {
		var errorMsg GenericError
		err := FromJSON(&errorMsg, resp.Body)
		if err != nil {
			body, _ := ioutil.ReadAll(resp.Body)
			u.l.Printf("[DEBUG] Unable to decode json. Body: %v", string(body))
			res.WriteHeader(http.StatusInternalServerError)
			return
		}
		// return to signin page
		u.l.Printf("ERR: %v\n", errorMsg.Message)
		u.redirectToSignUpPage(res, AuthError{true, errorMsg.Message})
		return
	}
	// redirect to signin
	http.Redirect(res, req, "/", 302)
	res.Header().Set("Location", "/")
}

func (u *Users) GoogleLogout(res http.ResponseWriter, req *http.Request) {
	gothic.Logout(res, req)
	res.Header().Set("Location", "/")
	res.WriteHeader(http.StatusTemporaryRedirect)
}

func (u *Users) GoogleAuthSignIn(res http.ResponseWriter, req *http.Request) {
	gothic.BeginAuthHandler(res, req)
}

func (u *Users) GoogleAuthSignUp(res http.ResponseWriter, req *http.Request) {
	gothic.BeginAuthHandler(res, req)
}
