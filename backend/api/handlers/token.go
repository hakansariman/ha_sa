package handlers

import (
	"fmt"

	"github.com/dgrijalva/jwt-go"
)

var (
	secretKey = []byte("t0k3nv3rys3cretk3y")
)

var ErrInvalidToken = fmt.Errorf("Invalid Token.")

// Create a struct that will be encoded to a JWT.
//  jwt.StandardClaims added as an embedded type, to provide fields like expiry time
type Claims struct {
	ID int `json:"id"`
	jwt.StandardClaims
}

func VerifyToken(tokenStr string) error {
	// Initialize a new instance of `Claims`
	claims := &Claims{}

	// Parse the JWT string and store the result in `claims`.
	// Note that we are passing the key in this method as well. This method will return an error
	// if the token is invalid (if it has expired according to the expiry time we set on sign in),
	// or if the signature does not match
	tkn, err := jwt.ParseWithClaims(tokenStr, claims, func(token *jwt.Token) (interface{}, error) {
		return secretKey, nil
	})
	if err != nil {
		return err
	}
	if !tkn.Valid {
		return ErrInvalidToken
	}

	return nil
}

func getUserIDFromToken(tokenStr string) (int, error) {
	token, _, err := new(jwt.Parser).ParseUnverified(tokenStr, jwt.MapClaims{})
	if err != nil {
		return -1, err
	}

	if _, ok := token.Claims.(jwt.MapClaims); !ok {
		return -1, fmt.Errorf("invalid token claims")
	}
	claims := token.Claims.(jwt.MapClaims)
	if _, ok := claims["id"]; !ok {
		return -1, fmt.Errorf("invalid token claims")
	}
	return int(claims["id"].(float64)), nil
}
