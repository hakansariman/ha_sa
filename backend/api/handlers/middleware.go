package handlers

import (
	"net/http"
	"strings"

	"../../data"
)

// MainMiddleware defines the main middleware, any other middlewares
// can be added to chain
var MainMiddleware = []middleware{
	LoggingMiddleware,
	AuthMiddleware,
}

// SignInMiddleware defines the main middleware, any other middlewares
// can be added to chain
var SignInMiddleware = []middleware{
	LoggingMiddleware,
}

// middleware is a definition of  what a middleware is,
// take in one handlerfunc and wrap it within another handlerfunc
type middleware func(http.HandlerFunc) http.HandlerFunc

// BuildMiddlewareChain builds the middleware chain recursively, functions are first class
func BuildMiddlewareChain(f http.HandlerFunc, m ...middleware) http.HandlerFunc {
	// if our chain is done, use the original handlerfunc
	if len(m) == 0 {
		return f
	}
	// otherwise nest the request handlers
	return m[0](BuildMiddlewareChain(f, m[1:len(m)]...))
}

// LoggingMiddleware - takes in a fasthttp.RequestHandler, and returns a fasthttp.RequestHandler
var LoggingMiddleware = func(f http.HandlerFunc) http.HandlerFunc {
	// one time scope setup area for middleware
	return func(w http.ResponseWriter, r *http.Request) {
		//log := fmt.Sprintf("[API] %q %q, IP: %q", ctx.Method(), ctx.Path(), ctx.RemoteIP())
		//logger.Info(log)
		// pass the other function
		f(w, r)
	}
}

// AuthMiddleware makes token authorization
var AuthMiddleware = func(f http.HandlerFunc) http.HandlerFunc {
	// one time scope setup area for middleware
	return func(w http.ResponseWriter, r *http.Request) {
		token := strings.Replace(r.Header.Get("Authorization"), "Bearer ", "", 1)
		if token == "" {
			// For any other type of error, return a bad request status
			w.WriteHeader(http.StatusBadRequest)
			data.ToJSON(&GenericError{Message: "Token not found."}, w)
			return
		}

		// Get the JWT string from the cookie
		err := VerifyToken(token)
		if err != nil {
			w.WriteHeader(http.StatusUnauthorized)
			data.ToJSON(&GenericError{Message: err.Error()}, w)
			return
		}
		// pass the other function
		f(w, r)

	}
}
