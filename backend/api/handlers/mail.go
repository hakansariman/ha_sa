package handlers

import (
	"fmt"
	"net/smtp"
	"time"

	"../../data"
	"github.com/dgrijalva/jwt-go"
)

type MailProfile struct {
	Sender       string
	Password     string
	ServerAddr   string
	ServerDomain string
}

var DefaultMailProfile = MailProfile{
	Sender:       "gochallangetest@gmail.com",
	Password:     "zibrjjtsnvjuncvi",
	ServerAddr:   "smtp.gmail.com:587",
	ServerDomain: "smtp.gmail.com",
}

func SendPasswordResetMailToUser(user data.User, callbackAddr string) error {
	expiration := time.Now().Add(time.Hour * 24)
	claims := &Claims{
		ID: user.ID,
		StandardClaims: jwt.StandardClaims{
			// In JWT, the expiry time is expressed as unix milliseconds
			ExpiresAt: expiration.Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenStr, err := token.SignedString(secretKey)
	if err != nil {
		return fmt.Errorf("error creating token %v", err)
	}
	body := fmt.Sprintf(
		"Hi %v,\n This is your password request link. This link is active for next 24 hours.\n %v",
		user.FullName,
		fmt.Sprintf("http://%v/resetpassword?token=%v", callbackAddr, tokenStr),
	)
	msg := "From: " + DefaultMailProfile.Sender + "\n" +
		"To: " + user.Username + "\n" +
		"Subject: GoChallange Password Reset Request\n\n" +
		body
	err = smtp.SendMail(DefaultMailProfile.ServerAddr,
		smtp.PlainAuth("", DefaultMailProfile.Sender, DefaultMailProfile.Password, DefaultMailProfile.ServerDomain),
		DefaultMailProfile.Sender, []string{user.Username}, []byte(msg))

	if err != nil {
		return fmt.Errorf("error sending mail %v", err)
	}
	return nil
}
