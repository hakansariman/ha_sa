package handlers

import (
	"net/http"

	"../../data"
)

// swagger:route PUT /users users updateUser
// Update a users details
//
// responses:
//	201: noContentResponse
//  404: errorResponse
//  422: errorValidation

// Update handles PUT requests to update users
func (u *Users) Update(rw http.ResponseWriter, r *http.Request) {
	rw.Header().Add("Content-Type", "application/json")

	// fetch the user from the context
	var user data.User
	err := data.FromJSON(&user, r.Body)
	if err != nil {
		u.l.Println("[ERROR] updating users", err)

		rw.WriteHeader(http.StatusInternalServerError)
		data.ToJSON(&GenericError{Message: err.Error()}, rw)
		return
	}
	u.l.Println("[DEBUG] updating user id", user.ID)

	err = u.dataManager.UpdateUser(user)
	if err == data.ErrUserNotFound {
		u.l.Println("[ERROR] user not found", err)

		rw.WriteHeader(http.StatusNotFound)
		data.ToJSON(&GenericError{Message: "User not found in database"}, rw)
		return
	} else if err != nil {
		u.l.Println("[ERROR] updating user", err)

		rw.WriteHeader(http.StatusNotFound)
		data.ToJSON(&GenericError{Message: err.Error()}, rw)
		return
	}

	// write the no content success header
	rw.WriteHeader(http.StatusNoContent)
}
