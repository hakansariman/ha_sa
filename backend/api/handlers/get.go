package handlers

import (
	"net/http"
	"strings"

	"../../data"
)

// Return a list of users from the database
// responses:
//	200: usersResponse

// ListAll handles GET requests and returns all current users
func (u *Users) ListAll(rw http.ResponseWriter, r *http.Request) {
	u.l.Println("[DEBUG] get all users")
	rw.Header().Add("Content-Type", "application/json")

	users, err := u.dataManager.GetUsers()
	if err != nil {
		u.l.Println("[ERROR] fetching users", err)

		rw.WriteHeader(http.StatusInternalServerError)
		data.ToJSON(&GenericError{Message: err.Error()}, rw)
		return
	}

	err = data.ToJSON(users, rw)
	if err != nil {
		// we should never be here but log the error just incase
		u.l.Println("[ERROR] serializing user", err)
	}
}

// Return a list of users from the database
// responses:
//	200: userResponse
//	404: errorResponse

// ListSingle handles GET requests
func (u *Users) ListSingle(rw http.ResponseWriter, r *http.Request) {
	rw.Header().Add("Content-Type", "application/json")

	id := getUserID(r)

	tokenID, err := getUserIDFromToken(strings.Replace(r.Header.Get("Authorization"), "Bearer ", "", 1))
	if err != nil {
		u.l.Println("[ERROR] validating token", err)
		rw.WriteHeader(http.StatusUnauthorized)
		data.ToJSON(&GenericError{Message: err.Error()}, rw)
		return
	}
	if id != tokenID {
		u.l.Println("[ERROR] unauthorized access", err)
		rw.WriteHeader(http.StatusUnauthorized)
		data.ToJSON(&GenericError{Message: "Unauthorized access."}, rw)
		return
	}
	u.l.Println("[DEBUG] get record id", id)

	user, err := u.dataManager.GetUserByID(id)

	switch err {
	case nil:
	case data.ErrUserNotFound:
		u.l.Println("[ERROR] fetching user", err)

		rw.WriteHeader(http.StatusNotFound)
		data.ToJSON(&GenericError{Message: err.Error()}, rw)
		return
	default:
		u.l.Println("[ERROR] fetching user", err)

		rw.WriteHeader(http.StatusInternalServerError)
		data.ToJSON(&GenericError{Message: err.Error()}, rw)
		return
	}

	err = data.ToJSON(user, rw)
	if err != nil {
		// we should never be here but log the error just incase
		u.l.Println("[ERROR] serializing user", err)
	}
}
