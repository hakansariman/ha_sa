package handlers

import (
	"net/http"
	"time"

	"../../data"
	"github.com/dgrijalva/jwt-go"
)

// Create a new user
//
// responses:
//	200: userResponse
//  422: errorValidation
//  501: errorResponse

// SignIn handles POST requests to sign in
func (u *Users) SignIn(rw http.ResponseWriter, r *http.Request) {
	// fetch the user from the context
	var user data.User
	err := data.FromJSON(&user, r.Body)
	if err != nil {
		u.l.Println("[ERROR] login user", err)

		rw.WriteHeader(http.StatusInternalServerError)
		data.ToJSON(&GenericError{Message: err.Error()}, rw)
		return
	}

	u.l.Printf("[DEBUG] Sign In user: %#v\n", user)
	id, err := u.dataManager.Login(user)
	if err != nil {
		u.l.Println("[ERROR] login user", err)

		rw.WriteHeader(http.StatusInternalServerError)
		data.ToJSON(&GenericError{Message: err.Error()}, rw)
		return
	}
	expiration := time.Now().Add(time.Minute * 45)
	claims := &Claims{
		ID: id,
		StandardClaims: jwt.StandardClaims{
			// In JWT, the expiry time is expressed as unix milliseconds
			ExpiresAt: expiration.Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(secretKey)
	if err != nil {
		u.l.Println("[ERROR] creating token", err)

		rw.WriteHeader(http.StatusInternalServerError)
		data.ToJSON(&GenericError{Message: err.Error()}, rw)
		return
	}
	cookie := http.Cookie{Name: "token", Value: tokenString, Expires: expiration}
	http.SetCookie(rw, &cookie)
}
