package handlers

import (
	"net/http"
	"strings"

	"../../data"
)

// Create a new user
//
// responses:
//	200: userResponse
//  422: errorValidation
//  501: errorResponse

// Create handles POST requests to add new users
func (u *Users) Create(rw http.ResponseWriter, r *http.Request) {
	// fetch the user from the context
	var user data.User
	err := data.FromJSON(&user, r.Body)
	if err != nil {
		u.l.Println("[ERROR] inserting users", err)

		rw.WriteHeader(http.StatusInternalServerError)
		data.ToJSON(&GenericError{Message: err.Error()}, rw)
		return
	}

	u.l.Printf("[DEBUG] Inserting user: %#v\n", user)
	err = u.dataManager.AddUser(user)
	if err != nil {
		u.l.Println("[ERROR] inserting users", err)

		rw.WriteHeader(http.StatusInternalServerError)
		data.ToJSON(&GenericError{Message: err.Error()}, rw)
		return
	}
}

// ForgotPassword handles POST requests to sending reset password links
func (u *Users) ForgotPassword(rw http.ResponseWriter, r *http.Request) {
	// fetch the user from the context
	var pr data.PasswordReset
	err := data.FromJSON(&pr, r.Body)
	if err != nil {
		u.l.Println("[ERROR] reset password", err)
		rw.WriteHeader(http.StatusInternalServerError)
		data.ToJSON(&GenericError{Message: err.Error()}, rw)
		return
	}

	u.l.Printf("[DEBUG] Creating reset password link for: %#v\n", pr)

	user, err := u.dataManager.GetUserByUsername(pr.Username)
	switch err {
	case nil:
	case data.ErrUserNotFound:
		u.l.Println("[ERROR] fetching user", err)

		rw.WriteHeader(http.StatusNotFound)
		data.ToJSON(&GenericError{Message: err.Error()}, rw)
		return
	default:
		u.l.Println("[ERROR] fetching user", err)

		rw.WriteHeader(http.StatusInternalServerError)
		data.ToJSON(&GenericError{Message: err.Error()}, rw)
		return
	}
	err = SendPasswordResetMailToUser(user, pr.CallbackAddr)
	if err != nil {
		u.l.Println("[ERROR] sending reset mail to user", err)
		rw.WriteHeader(http.StatusNotFound)
		data.ToJSON(&GenericError{Message: err.Error()}, rw)
	}

}

// ForgotPassword handles POST requests to sending reset password links
func (u *Users) ResetPassword(rw http.ResponseWriter, r *http.Request) {
	// fetch the user from the context
	var pr data.PasswordReset
	err := data.FromJSON(&pr, r.Body)
	if err != nil {
		u.l.Println("[ERROR] reset password", err)
		rw.WriteHeader(http.StatusInternalServerError)
		data.ToJSON(&GenericError{Message: err.Error()}, rw)
		return
	}

	userID, err := getUserIDFromToken(strings.Replace(r.Header.Get("Authorization"), "Bearer ", "", 1))
	if err != nil {
		u.l.Println("[ERROR] validating token", err)
		rw.WriteHeader(http.StatusUnauthorized)
		data.ToJSON(&GenericError{Message: err.Error()}, rw)
		return
	}

	user, err := u.dataManager.GetUserByID(userID)

	switch err {
	case nil:
	case data.ErrUserNotFound:
		u.l.Println("[ERROR] fetching user", err)

		rw.WriteHeader(http.StatusNotFound)
		data.ToJSON(&GenericError{Message: err.Error()}, rw)
		return
	default:
		u.l.Println("[ERROR] fetching user", err)

		rw.WriteHeader(http.StatusInternalServerError)
		data.ToJSON(&GenericError{Message: err.Error()}, rw)
		return
	}
	if user.ID != userID {
		u.l.Println("[ERROR] unauthorized access.", err)
		rw.WriteHeader(http.StatusUnauthorized)
		data.ToJSON(&GenericError{Message: err.Error()}, rw)
		return
	}
	user.Password = pr.Password

	err = u.dataManager.UpdateUser(user)
	if err != nil {
		if err != data.ErrNoRowsAffected {
			u.l.Println("[ERROR] sending reset mail to user", err)
			rw.WriteHeader(http.StatusNotFound)
			data.ToJSON(&GenericError{Message: err.Error()}, rw)
		}
	}

}
