package data

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
	"golang.org/x/crypto/bcrypt"
)

// db config parameters
const (
	dbName         = "usersdb"
	dbUsername     = "root"
	dbPassword     = "4#%WtKnm24q$_A7x"
	dbIP           = "127.0.0.1"
	dbPort         = 3306
	usersTableName = "users"
)

// initDB performs database initializations
func initDB() (*sql.DB, error) {
	// create database connection
	db, err := sql.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:%v)/", dbUsername, dbPassword, dbIP, dbPort))
	if err != nil {
		return nil, err
	}
	// create database if not exists
	_, err = db.Exec(fmt.Sprintf("CREATE DATABASE IF NOT EXISTS %s", dbName))
	if err != nil {
		return nil, err
	}
	// use the created database
	_, err = db.Exec(fmt.Sprintf("USE %s", dbName))
	if err != nil {
		return nil, err
	}
	// create users table if not exists
	_, err = db.Exec(fmt.Sprintf(`CREATE TABLE IF NOT EXISTS %s (
		ID int(10) unsigned NOT NULL AUTO_INCREMENT,
		username varchar(255) DEFAULT NULL,
		password varchar(255)  DEFAULT NULL,
		fullname varchar(255) DEFAULT NULL,
		address varchar(255) DEFAULT NULL,
		phone varchar(255) DEFAULT NULL,
		isGoogleUser boolean DEFAULT FALSE,
		PRIMARY KEY (ID),
		UNIQUE KEY username (username)
	) ENGINE=MyISAM  AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;`, usersTableName))
	if err != nil {
		return nil, err
	}
	return db, nil
}

func Hash(password string) ([]byte, error) {
	return bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
}

func VerifyPassword(hashedPassword, password string) error {
	return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
}
