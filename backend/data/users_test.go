package data

import (
	"log"
	"os"
	"testing"
)

// TestNewManager tests the database init operations
func TestNewManager(t *testing.T) {
	_, err := NewManager(log.New(os.Stdout, "test", log.LstdFlags))
	if err != nil {
		t.Errorf("Error creating new data manager: %v\n", err)
	}
}

// TestGetUsers tests the fetching all user data
func TestGetUsers(t *testing.T) {
	dm, err := NewManager(log.New(os.Stdout, "test", log.LstdFlags))
	if err != nil {
		t.Errorf("Error creating new data manager: %v\n", err)
		return
	}
	_, err = dm.GetUsers()
	if err != nil {
		t.Errorf("Error creating new data manager: %v\n", err)
		return
	}

}

var TestUser = User{
	Username: "testing@mail.com",
	Password: "testpw",
	FullName: "Test Person",
	Address:  "Earth",
}

// TestUserOperations tests the user adding/updating/fetching/deleting operations
func TestUserOperations(t *testing.T) {
	dm, err := NewManager(log.New(os.Stdout, "test", log.LstdFlags))
	if err != nil {
		t.Errorf("Error creating new data manager: %v\n", err)
		return
	}
	// add user
	err = dm.AddUser(TestUser)
	if err != nil {
		t.Errorf("Error adding user to db: %v\n", err)
		return
	}
	// get by mail
	user, err := dm.GetUserByUsername(TestUser.Username)
	if err != nil {
		t.Errorf("Error getting user by mail: %v\n", err)
		return
	}
	// check user fields
	if user.Address != TestUser.Address || user.FullName != TestUser.FullName ||
		user.Password != TestUser.Password || user.Username != TestUser.Username {
		t.Errorf("Error fetching user data, wrong fields: (%#v) %#v\n", TestUser, user)
		return
	}
	// update user
	user.FullName = "Other Person"
	err = dm.UpdateUser(user)
	if err != nil {
		t.Errorf("Error updating user: %v\n", err)
		return
	}
	// get user by id
	userNew, err := dm.GetUserByID(user.ID)
	// check user fields
	if user.ID != userNew.ID || user.Address != userNew.Address || user.FullName != userNew.FullName ||
		user.Password != userNew.Password || user.Username != userNew.Username {
		t.Errorf("Error fetching user data, wrong fields: (%#v) %#v\n", user, userNew)
		return
	}
	// delete user
	err = dm.DeleteUser(userNew.ID)
	if err != nil {
		t.Errorf("Error deleting user: %v\n", err)
		return
	}
	// get user which is not exists
	userDeleted, err := dm.GetUserByID(user.ID)
	if err == nil {
		t.Errorf("Error deleting user: %#v\n", userDeleted)
		return
	}
	if err != ErrUserNotFound {
		t.Errorf("Error deleting user: %v\n", err)
	}
}
