package data

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"html"
	"log"
	"strings"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

// ErrUserNotFound is an error raised when a user can not be found in the database
var ErrUserNotFound = fmt.Errorf("User not found")

// ErrInvalidCredentials is an error raised when a users credential is not correct.
var ErrInvalidCredentials = fmt.Errorf("Invalid credentials")

var ErrNoRowsAffected = errors.New("no rows were affected")

type manager struct {
	db  *sql.DB
	lgr *log.Logger
}

// Manager defines the data operations related with user data
type Manager interface {
	GetUsers() ([]User, error)
	GetUserByID(int) (User, error)
	GetUserByUsername(string) (User, error)
	UpdateUser(User) error
	AddUser(User) error
	DeleteUser(int) error
	Close()
	Login(User) (int, error)
}

// NewManager creates a new UserManager instance
func NewManager(lgr *log.Logger) (Manager, error) {
	db, err := initDB()
	if err != nil {
		return nil, err
	}
	return manager{
		db:  db,
		lgr: lgr,
	}, nil
}

// User defines the structure for an API user
type User struct {
	// the id for the user
	//
	// required: false
	// min: 1
	ID int `json:"id"` // Unique identifier for the user

	// the username for this user
	//
	// required: true
	// max length: 255
	Username string `json:"username" validate:"required"`

	// the password for this user
	//
	// required: true
	// max length: 255
	Password string `json:"password" validate:"required"`

	// the fullname for this user
	//
	// required: false
	// max length: 255
	FullName string `json:"fullname"`

	// the address for this user
	//
	// required: false
	// max length: 255
	Address string `json:"address"`

	// the fullname for this user
	//
	// required: false
	// max length: 255
	Phone string `json:"phone"`

	// is user authenticated by Google API
	//
	// required: false
	IsGoogleUser bool `json:"isGoogleUser"`
}

// PasswordReset defines the structure for an reset password request
type PasswordReset struct {
	// the username for this user
	//
	// required: false
	// max length: 255
	Username string `json:"username" `

	Password string `json:"password"`

	// CallbackAddr is the IP:Port data which mail be created
	CallbackAddr string `json:"callbackaddr"`
}

// GetUsers returns all users from the database
func (m manager) GetUsers() ([]User, error) {
	// set context
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	var users []User

	query := fmt.Sprintf("SELECT ID, username, password,fullname,address,phone,isGoogleUser FROM %v ORDER BY ID", usersTableName)
	rows, err := m.db.QueryContext(ctx, query)
	if err != nil {
		return users, err
	}
	defer rows.Close()

	for rows.Next() {
		var user User
		err := rows.Scan(
			&user.ID,
			&user.Username,
			&user.Password,
			&user.FullName,
			&user.Address,
			&user.Phone,
			&user.IsGoogleUser,
		)
		if err != nil {
			return users, err
		}
		users = append(users, user)
	}

	if err = rows.Err(); err != nil {
		return users, err
	}
	return users, nil
}

// GetUserByID returns a single user which matches the id from the
// database.
// If a user is not found this function returns a UserNotFound error
func (m manager) GetUserByID(id int) (User, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	var user User

	query := fmt.Sprintf("SELECT ID, username, password, fullname, address, phone, isGoogleUser FROM %v WHERE ID = %v", usersTableName, id)

	row := m.db.QueryRowContext(ctx, query)

	err := row.Scan(
		&user.ID,
		&user.Username,
		&user.Password,
		&user.FullName,
		&user.Address,
		&user.Phone,
		&user.IsGoogleUser,
	)
	// return
	switch {
	case err == sql.ErrNoRows:
		return user, ErrUserNotFound
	case err != nil:
		return user, err
	default:
		return user, nil
	}
}

// GetUserByUsername returns a single user which matches the id from the
// database.
// If a user is not found this function returns a UserNotFound error
func (m manager) GetUserByUsername(username string) (User, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	var user User

	query := fmt.Sprintf("SELECT ID, username, password, fullname, address, phone, isGoogleUser FROM %v WHERE username = '%v'", usersTableName, username)

	row := m.db.QueryRowContext(ctx, query)

	err := row.Scan(
		&user.ID,
		&user.Username,
		&user.Password,
		&user.FullName,
		&user.Address,
		&user.Phone,
		&user.IsGoogleUser,
	)
	// return
	switch {
	case err == sql.ErrNoRows:
		return user, ErrUserNotFound
	case err != nil:
		return user, err
	default:
		return user, nil
	}
}

// Login checks if given credentials exists int
// database.
// If user found in database, it's ID will be returned.
// If user not found in database, ErrUserNotFound will be returned.
// If credentials is not correct, ErrInvalidCredentials will be returned.
func (m manager) Login(user User) (int, error) {

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	var userDB User

	query := fmt.Sprintf("SELECT ID, username, password FROM %v WHERE username = \"%v\"", usersTableName, user.Username)

	row := m.db.QueryRowContext(ctx, query)

	err := row.Scan(
		&userDB.ID,
		&userDB.Username,
		&userDB.Password,
	)
	// return
	switch {
	case err == sql.ErrNoRows:
		return -1, ErrUserNotFound
	case err != nil:
		return -1, err
	default:
		// pass for Google Users
		if user.IsGoogleUser {
			return userDB.ID, nil
		}
		if userDB.Password != user.Password {
			return -1, ErrInvalidCredentials
		}
		return userDB.ID, nil
	}
}

// UpdateUser replaces a user in the database with the given
// item.
// If a user with the given id does not exist in the database
// this function returns a UserNotFound error
func (m manager) UpdateUser(u User) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	query := fmt.Sprintf(`
	UPDATE  %v SET username = ?, password = ?,fullname = ?,address = ?,phone = ? WHERE ID = %v`, usersTableName, u.ID)
	stmt, err := m.db.PrepareContext(ctx, query)
	if err != nil {
		return err
	}
	defer stmt.Close()

	res, err := stmt.ExecContext(ctx, u.Username, u.Password, u.FullName, u.Address, u.Phone)
	if err != nil {
		return err
	}

	tot, err := res.RowsAffected()
	if err != nil {
		return err
	}
	if tot != 1 {
		return ErrNoRowsAffected
	}
	return nil
}

// AddUser adds a new user to the database
func (m manager) AddUser(u User) error {
	u.Prepare()

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	query := fmt.Sprintf(`
	INSERT INTO %v
	(username, password, fullname, address, phone, isGoogleUser)
	VALUES
	(?, ?, ?, ?, ?, ?)
`, usersTableName)
	stmt, err := m.db.PrepareContext(ctx, query)
	if err != nil {
		return err
	}
	defer stmt.Close()

	res, err := stmt.ExecContext(ctx, u.Username, u.Password, u.FullName, u.Address, u.Phone, u.IsGoogleUser)
	if err != nil {
		return err
	}

	tot, err := res.RowsAffected()
	if err != nil {
		return err
	}
	if tot != 1 {
		return ErrNoRowsAffected
	}
	return nil
}

// DeleteUser deletes a user from the database
func (m manager) DeleteUser(id int) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	query := fmt.Sprintf("DELETE FROM %v WHERE ID = %v", usersTableName, id)

	stmt, err := m.db.PrepareContext(ctx, query)
	if err != nil {
		return err
	}

	res, err := stmt.ExecContext(ctx)
	if err != nil {
		return err
	}

	tot, err := res.RowsAffected()
	if err != nil {
		return err
	}
	if tot != 1 {
		return ErrNoRowsAffected
	}
	return nil
}

func (u *User) Prepare() error {
	u.ID = 0
	u.Username = html.EscapeString(strings.TrimSpace(u.Username))
	//hashedPassword, err := Hash(u.Password)
	//if err != nil {
	//	return err
	//}
	//u.Password = string(hashedPassword)
	return nil
}

func (u *User) Validate(action string) error {
	switch strings.ToLower(action) {
	case "update":
		if u.Password == "" {
			return errors.New("Required Password")
		}
		if u.Username == "" {
			return errors.New("Required Username")
		}
		return nil
	case "login":
		if u.Password == "" {
			return errors.New("Required Password")
		}
		if u.Username == "" {
			return errors.New("Required Username")
		}
		return nil

	default:
		if u.Password == "" {
			return errors.New("Required Password")
		}
		if u.Username == "" {
			return errors.New("Required Username")
		}
		return nil
	}
}

// Close closes db connection
func (m manager) Close() {
	m.db.Close()
}
