package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"./api/handlers"
	gorhandlers "github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

func main() {

	l := log.New(os.Stdout, "users-api ", log.LstdFlags)

	// create the handlers
	uh, err := handlers.NewUsers(l)
	if err != nil {
		panic(err)
	}

	// create a new serve mux and register the handlers
	sm := mux.NewRouter()

	// handlers for API
	getR := sm.Methods(http.MethodGet).Subrouter()
	getR.HandleFunc("/users", handlers.BuildMiddlewareChain(uh.ListAll, handlers.MainMiddleware...))
	getR.HandleFunc("/users/{id:[0-9]+}", handlers.BuildMiddlewareChain(uh.ListSingle, handlers.MainMiddleware...))

	putR := sm.Methods(http.MethodPut).Subrouter()
	putR.HandleFunc("/users", handlers.BuildMiddlewareChain(uh.Update, handlers.MainMiddleware...))

	postR := sm.Methods(http.MethodPost).Subrouter()
	postR.HandleFunc("/users", handlers.BuildMiddlewareChain(uh.Create, handlers.SignInMiddleware...))
	postR.HandleFunc("/signin", handlers.BuildMiddlewareChain(uh.SignIn, handlers.SignInMiddleware...))
	postR.HandleFunc("/forgotpassword", handlers.BuildMiddlewareChain(uh.ForgotPassword, handlers.SignInMiddleware...))
	postR.HandleFunc("/resetpassword", handlers.BuildMiddlewareChain(uh.ResetPassword, handlers.SignInMiddleware...))

	deleteR := sm.Methods(http.MethodDelete).Subrouter()
	deleteR.HandleFunc("/users/{id:[0-9]+}", handlers.BuildMiddlewareChain(uh.Delete, handlers.MainMiddleware...))

	// CORS
	ch := gorhandlers.CORS(gorhandlers.AllowedOrigins([]string{"*"}))

	// create a new server
	s := http.Server{
		Addr:         handlers.BindAddress, // configure the bind address
		Handler:      ch(sm),               // set the default handler
		ErrorLog:     l,                    // set the logger for the server
		ReadTimeout:  5 * time.Second,      // max time to read request from the client
		WriteTimeout: 10 * time.Second,     // max time to write response to the client
		IdleTimeout:  120 * time.Second,    // max time for connections using TCP Keep-Alive
	}

	// start the server
	go func() {
		l.Println("Starting server on port 9090")

		err := s.ListenAndServe()
		if err != nil {
			l.Printf("Error starting server: %s\n", err)
			os.Exit(1)
		}
	}()

	// trap sigterm or interupt and gracefully shutdown the server
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	signal.Notify(c, os.Kill)

	// Block until a signal is received.
	sig := <-c
	log.Println("Got signal:", sig)

	// gracefully shutdown the server, waiting max 30 seconds for current operations to complete
	ctx, _ := context.WithTimeout(context.Background(), 30*time.Second)
	s.Shutdown(ctx)
	uh.Close()
}
